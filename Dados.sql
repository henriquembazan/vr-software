PGDMP         ,    
            y            Dados    10.16    10.16 $               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    16393    Dados    DATABASE     �   CREATE DATABASE "Dados" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Portuguese_Brazil.1252' LC_CTYPE = 'Portuguese_Brazil.1252';
    DROP DATABASE "Dados";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    24578    tb_aluno    TABLE     �   CREATE TABLE public.tb_aluno (
    id integer NOT NULL,
    nome character varying NOT NULL,
    rg character varying NOT NULL,
    cpf character varying NOT NULL
);
    DROP TABLE public.tb_aluno;
       public         postgres    false    3            �            1259    24576    tb_aluno_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tb_aluno_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.tb_aluno_id_seq;
       public       postgres    false    199    3                       0    0    tb_aluno_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.tb_aluno_id_seq OWNED BY public.tb_aluno.id;
            public       postgres    false    198            �            1259    24600    tb_curso    TABLE     �   CREATE TABLE public.tb_curso (
    id integer NOT NULL,
    descricao character varying NOT NULL,
    duracao integer NOT NULL,
    periodo character varying NOT NULL,
    alunos integer NOT NULL,
    carga integer NOT NULL
);
    DROP TABLE public.tb_curso;
       public         postgres    false    3            �            1259    24598    tb_curso_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tb_curso_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.tb_curso_id_seq;
       public       postgres    false    3    203                       0    0    tb_curso_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.tb_curso_id_seq OWNED BY public.tb_curso.id;
            public       postgres    false    202            �            1259    24589    tb_disciplina    TABLE     �   CREATE TABLE public.tb_disciplina (
    id integer NOT NULL,
    disciplina character varying NOT NULL,
    diasemana character varying NOT NULL,
    periodo character varying NOT NULL
);
 !   DROP TABLE public.tb_disciplina;
       public         postgres    false    3            �            1259    24587    tb_disciplina_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tb_disciplina_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tb_disciplina_id_seq;
       public       postgres    false    3    201                       0    0    tb_disciplina_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tb_disciplina_id_seq OWNED BY public.tb_disciplina.id;
            public       postgres    false    200            �            1259    16396    tb_professor    TABLE     �   CREATE TABLE public.tb_professor (
    id integer NOT NULL,
    nome character varying NOT NULL,
    rg character varying NOT NULL,
    cpf character varying NOT NULL,
    titulo character varying NOT NULL
);
     DROP TABLE public.tb_professor;
       public         postgres    false    3            �            1259    16394    tb_professor_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tb_professor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.tb_professor_id_seq;
       public       postgres    false    3    197                       0    0    tb_professor_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.tb_professor_id_seq OWNED BY public.tb_professor.id;
            public       postgres    false    196            �
           2604    24581    tb_aluno id    DEFAULT     j   ALTER TABLE ONLY public.tb_aluno ALTER COLUMN id SET DEFAULT nextval('public.tb_aluno_id_seq'::regclass);
 :   ALTER TABLE public.tb_aluno ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199            �
           2604    24603    tb_curso id    DEFAULT     j   ALTER TABLE ONLY public.tb_curso ALTER COLUMN id SET DEFAULT nextval('public.tb_curso_id_seq'::regclass);
 :   ALTER TABLE public.tb_curso ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    202    203            �
           2604    24592    tb_disciplina id    DEFAULT     t   ALTER TABLE ONLY public.tb_disciplina ALTER COLUMN id SET DEFAULT nextval('public.tb_disciplina_id_seq'::regclass);
 ?   ALTER TABLE public.tb_disciplina ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201            �
           2604    16399    tb_professor id    DEFAULT     r   ALTER TABLE ONLY public.tb_professor ALTER COLUMN id SET DEFAULT nextval('public.tb_professor_id_seq'::regclass);
 >   ALTER TABLE public.tb_professor ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197                      0    24578    tb_aluno 
   TABLE DATA               5   COPY public.tb_aluno (id, nome, rg, cpf) FROM stdin;
    public       postgres    false    199   �$                 0    24600    tb_curso 
   TABLE DATA               R   COPY public.tb_curso (id, descricao, duracao, periodo, alunos, carga) FROM stdin;
    public       postgres    false    203   �$                 0    24589    tb_disciplina 
   TABLE DATA               K   COPY public.tb_disciplina (id, disciplina, diasemana, periodo) FROM stdin;
    public       postgres    false    201   #%       
          0    16396    tb_professor 
   TABLE DATA               A   COPY public.tb_professor (id, nome, rg, cpf, titulo) FROM stdin;
    public       postgres    false    197   c%                  0    0    tb_aluno_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.tb_aluno_id_seq', 1, true);
            public       postgres    false    198                       0    0    tb_curso_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.tb_curso_id_seq', 1, true);
            public       postgres    false    202                       0    0    tb_disciplina_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tb_disciplina_id_seq', 2, true);
            public       postgres    false    200                        0    0    tb_professor_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tb_professor_id_seq', 10, true);
            public       postgres    false    196            �
           2606    24586    tb_aluno tb_aluno_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.tb_aluno
    ADD CONSTRAINT tb_aluno_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.tb_aluno DROP CONSTRAINT tb_aluno_pkey;
       public         postgres    false    199            �
           2606    24608    tb_curso tb_curso_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.tb_curso
    ADD CONSTRAINT tb_curso_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.tb_curso DROP CONSTRAINT tb_curso_pkey;
       public         postgres    false    203            �
           2606    24597     tb_disciplina tb_disciplina_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tb_disciplina
    ADD CONSTRAINT tb_disciplina_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.tb_disciplina DROP CONSTRAINT tb_disciplina_pkey;
       public         postgres    false    201            �
           2606    16404    tb_professor tb_professor_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tb_professor
    ADD CONSTRAINT tb_professor_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tb_professor DROP CONSTRAINT tb_professor_pkey;
       public         postgres    false    197               .   x�3��p��u�41�4���02�410�0003��4����� �t~         $   x�3�qq�4��u	����42 "�=... g]          0   x�3��uqbOgG�`W�P?G�Ph���?�g�kp�+�D� ��      
   F   x�34���v�41�4���02�410�0003��4��u	rt��2��p��ť��?4��4F��� -Pu     